FROM elasticsearch:5.6-alpine as elasticsearch

RUN cd /usr/share/elasticsearch && \
      bin/elasticsearch-plugin install analysis-phonetic && \
      bin/elasticsearch-plugin install analysis-icu
